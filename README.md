# nrec-create-server

A role for creating servers in the Openstack based nrec. For more info, see:

Install on command line with `ansible-galaxy install git+https://git.app.uib.no/itpublic/ansible/roles/nrec-create-server.git`, or include in requirements.yml to install with `ansible-galaxy install -r requirements.yml -p ~/.ansible/roles/`:
```
- src: git+https://git.app.uib.no/itpublic/ansible/roles/nrec-create-server.git
```



## Requirements
You will need API access to nrec. In order to connect to nrec ansible will use the python application openstacksdk. openstacksdk will need your credentials in a `clouds.yaml` file _or_ sourced into your environment from a keystone_rc file.

### clouds.yaml
This ansible role https://git.app.uib.no/itpublic/ansible/roles/nrec-create-clouds-yaml-file will help you when creating your `clouds.yaml` file. 

### keystone_rc

```
$ cat $HOME/keystone_rc
export OS_AUTH_URL="https://api.nrec.no:5000/v3"
export OS_IDENTITY_API_VERSION=3
export OS_USERNAME="username"
export OS_PASSWORD="password"
export OS_USER_DOMAIN_NAME=dataporten
export OS_PROJECT_DOMAIN_NAME=dataporten
export OS_REGION_NAME=bgo
export OS_NO_CACHE=1
export OS_PROJECT_NAME="projectname"
```

To do this, source this file like this:
```
source $HOME/keystone_rc
```


Also, since your credentials to the API resides in the file, make sure no one else can read or execute it:
```
chmod 700 $HOME/keystone_rc.sh
```

For more info, check out the nrec documentation: http://docs.nrec.no/en/latest

Role Variables
--------------

All required variables are set in `defaults/main.yml`. Override them in your own `vars/main.yml`, `group_vars/groupname.yml`, `host_vars/hostname.yml` or similar. Define your ssh private key as you run:
```
ansible-playbook site.yaml --private-key /home/username/.ssh/cloud-api
``` 

Dependencies
------------

    openstacksdk
    openstacksdk >= 0.12.0
    python >= 2.7


Example Playbook
----------------

Because your project and credentials are defined by sourcing the keystone-file into your env, you can start servers with a simple variables in your playbook or in a variable file. This sets up two servers, but only installs someapplication on one of them:

    - name: create instances
      hosts: localhost
      vars:
        prefix: demo
        servers:
          - name: "someapplication01"
            key_name: "my-cloud-key"
            meta:
              groups: "nrec"
          - name: "someapplication02"
            key_name: "my-cloud-key"
            meta:
              groups: "nrec, someapplication"
      roles:
        - { role: nrec-create-server }

    
    - hosts: someapplication
      become: true
      roles:
        - { role: someansiblegalaxydeveloper.someapplication }

    

License
-------

BSD

Author Information
------------------

Author: See git log. :)

